

import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget{

  const Assignment1({super.key});

  @override
  State<Assignment1> createState() => _Assignment1State();

}

class _Assignment1State extends State<Assignment1>{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        backgroundColor: Colors.white,
        title: const Text(" Instagram ",
            style: TextStyle(
              fontStyle: FontStyle.italic,
              fontSize: 30,
              color: Colors.black,
            ),
          ),
          actions: const [
              Icon(  
                Icons.favorite,
                color: Colors.red,
                
              ),  
          ],
      ),

      body: 

      SingleChildScrollView(
        child:  
          Column(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                    Container(
                        color: const Color.fromARGB(255, 248, 244, 53),
                        child :Image.network(
                          "https://static.vecteezy.com/system/resources/thumbnails/024/669/482/small/mountain-countryside-landscape-at-sunset-dramatic-sky-over-a-distant-valley-green-fields-and-trees-on-hill-beautiful-natural-landscapes-of-the-carpathians-generative-ai-variation-8-photo.jpeg",
                          width: double.infinity,
                          height: 200,
                        ),
                      ),
                    Row(
                      children: [
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.favorite_outline_outlined,
                            ),
                          ),
                          
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.comment_outlined,
                            ),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.send_rounded,
                            ),
                          ),

                          const SizedBox(
                            width: 155,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.save_alt_outlined,
                            ),
                          ),


                      ],
                      
                      )
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                  Container(
                    color: const Color.fromARGB(255, 53, 248, 235),
                    child :Image.network(
                      "https://static.vecteezy.com/system/resources/thumbnails/024/669/482/small/mountain-countryside-landscape-at-sunset-dramatic-sky-over-a-distant-valley-green-fields-and-trees-on-hill-beautiful-natural-landscapes-of-the-carpathians-generative-ai-variation-8-photo.jpeg",
                      width: double.infinity,
                      height: 200,
                    ),
                    ),
                    Row(
                      children: [
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.favorite_outline_outlined,
                            ),
                          ),
                          
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.comment_outlined,
                            ),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.send_rounded,
                            ),
                          ),
                          const SizedBox(
                            width: 155,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.save_alt_outlined,
                            ),
                          ),


                      ],
                      
                      )
                  ],
                ),

                const SizedBox(
                  height: 5,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                  Container(
                    color: const Color.fromARGB(255, 250, 100, 255),
                    child :Image.network(
                      "https://static.vecteezy.com/system/resources/thumbnails/024/669/482/small/mountain-countryside-landscape-at-sunset-dramatic-sky-over-a-distant-valley-green-fields-and-trees-on-hill-beautiful-natural-landscapes-of-the-carpathians-generative-ai-variation-8-photo.jpeg",
                      width: double.infinity,
                      height: 200,
                    ),
                    ),
                    Row(
                      children: [
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.favorite_outline_outlined,
                            ),
                          ),
                          
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.comment_outlined,
                            ),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.send_rounded,
                            ),
                          ),
                          const SizedBox(
                            width: 155,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.save_alt_outlined,
                            ),
                          ),


                      ],
                      
                      )
                  ],
                ),
              ],
          ),
      ),
    );
  }

}