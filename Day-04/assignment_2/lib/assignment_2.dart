

import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget{

  const Assignment2({super.key});

  @override
  State<Assignment2> createState() => _Assignment2State();

}

class _Assignment2State extends State<Assignment2>{

  
  bool _postLiked1 = false;
  bool _postLiked2 = false;
  bool _postLiked3 = false;
  bool _bookmarked1 = false;
  bool _bookmarked2 = false;
  bool _bookmarked3 = false;

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        backgroundColor: Colors.white,
        title: const Text(" Instagram ",
            style: TextStyle(
              fontStyle: FontStyle.italic,
              fontSize: 30,
              color: Colors.black,
            ),
          ),
          actions: const [
              Icon(  
                Icons.comment_rounded,
                color: Colors.black,
                
              ),  
          ],
      ),

      body: SingleChildScrollView(
        child:  
          Column(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                    Container(
                        color: const Color.fromARGB(255, 248, 244, 53),
                        child :Image.network(
                          "https://static.vecteezy.com/system/resources/thumbnails/024/669/482/small/mountain-countryside-landscape-at-sunset-dramatic-sky-over-a-distant-valley-green-fields-and-trees-on-hill-beautiful-natural-landscapes-of-the-carpathians-generative-ai-variation-8-photo.jpeg",
                          width: double.infinity,
                          height: 200,
                        ),
                      ),
                    Row(
                      
                      children: [
                          
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _postLiked1 =!_postLiked1;
                              });
                            } ,
                            icon: _postLiked1 ? const Icon(
                              Icons.favorite_border_rounded,
                            ) : const Icon(
                              Icons.favorite_rounded,
                              color: Colors.red,
                            )
                          ),
                          
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.comment_outlined,
                            ),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.send_rounded,
                            ),
                          ),
                          
                          const Spacer(),
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _bookmarked1 =!_bookmarked1;
                              });
                            } ,
                            icon: _bookmarked1 ? const Icon(
                              Icons.bookmark_add_outlined,
                            ) : const Icon(
                              Icons.bookmark_rounded,
                              color: Color.fromARGB(255, 6, 6, 6),
                            )
                          ),
                      ],            
                    )
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                  Container(
                    color: const Color.fromARGB(255, 53, 248, 235),
                    child :Image.network(
                      "https://static.vecteezy.com/system/resources/thumbnails/024/669/482/small/mountain-countryside-landscape-at-sunset-dramatic-sky-over-a-distant-valley-green-fields-and-trees-on-hill-beautiful-natural-landscapes-of-the-carpathians-generative-ai-variation-8-photo.jpeg",
                      width: double.infinity,
                      height: 200,
                    ),
                    ),
                    Row(
                      children: [
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _postLiked2 =!_postLiked2;
                              });
                            } ,
                            icon: _postLiked2 ? const Icon(
                              Icons.favorite_border_rounded,
                            ) : const Icon(
                              Icons.favorite_rounded,
                              color: Colors.red,
                            )
                          ),
                          
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.comment_outlined,
                            ),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.send_rounded,
                            ),
                          ),
                          const Spacer(),
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _bookmarked2 =!_bookmarked2;
                              });
                            } ,
                            icon: _bookmarked2 ? const Icon(
                              Icons.bookmark_add_outlined,
                            ) : const Icon(
                              Icons.bookmark_rounded,
                              color: Color.fromARGB(255, 6, 6, 6),
                            )
                          ),


                      ],
                      
                      )
                  ],
                ),

                const SizedBox(
                  height: 5,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                  Container(
                    color: const Color.fromARGB(255, 250, 100, 255),
                    child :Image.network(
                      "https://static.vecteezy.com/system/resources/thumbnails/024/669/482/small/mountain-countryside-landscape-at-sunset-dramatic-sky-over-a-distant-valley-green-fields-and-trees-on-hill-beautiful-natural-landscapes-of-the-carpathians-generative-ai-variation-8-photo.jpeg",
                      width: double.infinity,
                      height: 200,
                    ),
                    ),
                    Row(
                      children: [
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _postLiked3 =!_postLiked3;
                              });
                            } ,
                            icon: _postLiked3 ? const Icon(
                              Icons.favorite_border_rounded,
                            ) : const Icon(
                              Icons.favorite_rounded,
                              color: Colors.red,
                            )
                          ),
                          
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.comment_outlined,
                            ),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.send_rounded,
                            ),
                          ),
                          const Spacer(),
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _bookmarked3 =!_bookmarked3;
                              });
                            } ,
                            icon: _bookmarked3 ? const Icon(
                              Icons.bookmark_add_outlined,
                            ) : const Icon(
                              Icons.bookmark_rounded,
                              color: Color.fromARGB(255, 6, 6, 6),
                            )
                          ),


                      ],
                      
                      )
                  ],
                ),
              ],
          ),
      ),
    );
  }

}