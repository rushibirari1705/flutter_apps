

import 'package:flutter/material.dart';

class InstaClone extends StatefulWidget{

  const InstaClone({super.key});

  @override
  State<InstaClone> createState() => _InstaCloneState();

}

class _InstaCloneState extends State<InstaClone>{

  
  bool _postLiked1 = true;
  bool _postLiked2 = true;
  bool _postLiked3 = true;
  bool _bookmarked1 = true;
  bool _bookmarked2 = true;
  bool _bookmarked3 = true;

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        backgroundColor: Colors.white,
        title: const Text(" Instagram ",
            style: TextStyle(
              fontSize: 30,
              color: Colors.black,
              fontFamily: 'Billabong',
            ),
          ),
          actions: const [
                Icon(  
                  Icons.favorite_outline_outlined,
                  color: Colors.black,
                ),
      
              SizedBox(
                width: 15,
              ),
              Icon(  
                  Icons.maps_ugc_rounded,
                  color: Colors.black,

                ),
                
              SizedBox(
                width: 15,
              ),

              
          ],
      ),

      body: SingleChildScrollView(
        child:  
          Column(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                    Container(
                        color: const Color.fromARGB(255, 255, 255, 255),
                        child :Image.network(
                          "https://cdn.pixabay.com/photo/2017/03/27/14/56/auto-2179220_640.jpg",
                          width: double.infinity,
                          height: 200,
                        ),
                      ),
                    Row(
                      
                      children: [
                          
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _postLiked1 =!_postLiked1;
                              });
                            } ,
                            icon: _postLiked1 ? const Icon(
                              Icons.favorite_border_rounded,
                            ) : const Icon(
                              Icons.favorite_rounded,
                              color: Colors.red,
                            )
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.chat_bubble_outline_outlined,
                            ),
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.send_rounded,
                            ),
                          ),
                          
                          const Spacer(),
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _bookmarked1 =!_bookmarked1;
                              });
                            } ,
                            icon: _bookmarked1 ? const Icon(
                              Icons.bookmark_add_outlined,
                            ) : const Icon(
                              Icons.bookmark_rounded,
                              color: Color.fromARGB(255, 6, 6, 6),
                            )
                          ),
                      ],            
                    )
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                  Container(
                    color: const  Color.fromARGB(255, 255, 255, 255),
                    child :Image.network(
                      "https://images.unsplash.com/photo-1542362567-b07e54358753?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fGNhcnN8ZW58MHx8MHx8fDA%3D",
                      width: double.infinity,
                      height: 200,
                    ),
                    ),
                    Row(
                      children: [
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _postLiked2 =!_postLiked2;
                              });
                            } ,
                            icon: _postLiked2 ? const Icon(
                              Icons.favorite_border_rounded,
                            ) : const Icon(
                              Icons.favorite_rounded,
                              color: Colors.red,
                            )
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.chat_bubble_outline_sharp,
                            ),
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.send_rounded,
                            ),
                          ),
                          const Spacer(),
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _bookmarked2 =!_bookmarked2;
                              });
                            } ,
                            icon: _bookmarked2 ? const Icon(
                              Icons.bookmark_add_outlined,
                            ) : const Icon(
                              Icons.bookmark_rounded,
                              color: Color.fromARGB(255, 6, 6, 6),
                            )
                          ),


                      ],
                      
                      )
                  ],
                ),

                const SizedBox(
                  height: 3,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                  Container(
                    color: const Color.fromARGB(255, 255, 255, 255),
                    child :Image.network(
                      "https://stimg.cardekho.com/images/carexteriorimages/930x620/Mclaren/750-s/9929/1682577543178/exterior-image-166.jpg?impolicy=resize&imwidth=420",
                      width: double.infinity,
                      height: 200,
                    ),
                    ),
                    Row(
                      children: [
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _postLiked3 =!_postLiked3;
                              });
                            } ,
                            icon: _postLiked3 ? const Icon(
                              Icons.favorite_border_rounded,
                            ) : const Icon(
                              Icons.favorite_rounded,
                              color: Colors.red,
                            )
                          ),
                        
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.chat_bubble_outline_sharp,
                            ),
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.send_rounded,
                            ),
                          ),
                          const Spacer(),
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _bookmarked3 =!_bookmarked3;
                              });
                            } ,
                            icon: _bookmarked3 ? const Icon(
                              Icons.bookmark_add_outlined,
                            ) : const Icon(
                              Icons.bookmark_rounded,
                              color: Color.fromARGB(255, 6, 6, 6),
                            )
                          ),
                      ],
                      
                      )
                  ],

                  
                ),

                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                  Container(
                    color: const Color.fromARGB(255, 255, 255, 255),
                    child :Image.network(
                      "https://images.unsplash.com/photo-1612468008274-9445bd56161e?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8Y29vbCUyMGNhcnxlbnwwfHwwfHx8MA%3D%3D",
                      width: double.infinity,
                      height: 200,
                    ),
                    ),
                    Row(
                      children: [
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _postLiked3 =!_postLiked3;
                              });
                            } ,
                            icon: _postLiked3 ? const Icon(
                              Icons.favorite_border_rounded,
                            ) : const Icon(
                              Icons.favorite_rounded,
                              color: Colors.red,
                            )
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.chat_bubble_outline_sharp,
                            ),
                          ),
                          IconButton(
                            onPressed: (){}, 
                            icon: const Icon(
                              Icons.send_rounded,
                            ),
                          ),
                          const Spacer(),
                          IconButton(
                            onPressed: (){
                              setState(() {
                                _bookmarked3 =!_bookmarked3;
                              });
                            } ,
                            icon: _bookmarked3 ? const Icon(
                              Icons.bookmark_add_outlined,
                            ) : const Icon(
                              Icons.bookmark_rounded,
                              color: Color.fromARGB(255, 6, 6, 6),
                            )
                          ),
                      ],
                      
                      )
                  ],

                  
                ),
              ],
          ),
      ),

      bottomNavigationBar: BottomNavigationBar(
        items:const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
            backgroundColor: Color.fromARGB(255, 255, 138, 138),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'search',
            backgroundColor: Color.fromARGB(255, 108, 248, 231),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_circle_outlined),
            label: 'add',
            backgroundColor: Color.fromARGB(255, 255, 130, 236),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_add_alt_1),
            label: 'profile',
            backgroundColor: Color.fromARGB(255, 108, 248, 127),
          ),

        ]
        ),
    );
  }

}
