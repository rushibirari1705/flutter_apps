
import 'package:flutter/material.dart';

void main() => runApp(const MyButton());

class MyButton extends StatelessWidget{

  const MyButton({super.key});

    @override
    Widget build(BuildContext context){
      return MaterialApp(
        home: Scaffold(
          appBar:  AppBar(
            title: const Text("Home Page"),
          ),
        body: OutlinedButton(
          child: const Text("Click me! "),
          onPressed: (){
            print("Hello here..!");
            increment();
            
          },
          onLongPress: () => print("Hello long pressed..."),

          
        ),

        ),
      );
    }


    MaterialApp increment() {
      
      return MaterialApp(
        home: Scaffold(
          appBar:AppBar(
            title: const Text ("Hello increment")
          ),
        body: ElevatedButton(
          child: const Text("Elevated Button"),
          onPressed: (){},
            )
          )
        
      );
      
    }
}