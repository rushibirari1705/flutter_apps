

import 'package:flutter/material.dart';

class NetflixClone extends StatelessWidget {

  const NetflixClone({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 37, 37, 37),
      appBar: AppBar(
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                right:20,
              ),
              child:
              Image.asset(
                  "assets/netflix_logo.png",
                  width: 65,
                  height: 45,
                ),
              ),
              const SizedBox(
                width: 5,
              ),

              Image.network(
                "https://images.ctfassets.net/y2ske730sjqp/1aONibCke6niZhgPxuiilC/2c401b05a07288746ddf3bd3943fbc76/BrandAssets_Logos_01-Wordmark.jpg?w=940",
                //width: 300,
                height: 75,
              ),
              ],
              ),
          backgroundColor:const Color.fromARGB(255, 234, 225, 225),
        ),
      
      body:SingleChildScrollView(child: 
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(
                height: 5,
              ),
            const Row(
              children: [
                Text(" Movies" , style: TextStyle(
                fontWeight: FontWeight.normal,
                fontFamily: 'Robotomono',
                color: Colors.white,

              ),
            ),
              ],
            ),
            const SizedBox(
              height: 5,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: 
                SingleChildScrollView(
                child: 
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 5,
                      ),
                                  
                      Image.network(
                        "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg",
                        height:250,
                        width: 200,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Image.network(
                        "https://filmfare.wwmindia.com/content/2022/dec/pathaan11669878737.jpg",
                        height:250,
                        width: 200,

                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Image.network(
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUkW6rTBzfzXWPMK0I6lIgQtTAdmXiOLLoz1Q813eYC8VIepHCnquZsnZUUaUtcPjqfjA&usqp=CAU",
                        height:250,
                        width: 200,
                      
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Image.network(
                        "https://mir-s3-cdn-cf.behance.net/project_modules/hd/3a62ce49135139.58ac2c8de7a2c.jpg",
                        height:250,
                        width: 200,

                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Image.network(
                        "https://c4.wallpaperflare.com/wallpaper/852/644/1008/alien-movie-poster-sigourney-weaver-movie-poster-wallpaper-preview.jpg",
                        height:250,
                        width: 200,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Image.network(
                        "https://i.pinimg.com/564x/02/2c/8c/022c8c3ce9953af6e2daf2b95de33d37--jennifer-lawrence.jpg",
                        height:250,
                        width: 200,
                      ),
                      
                    ],
                  ),

                  ),

              ),

            const SizedBox(
                height: 10,
              ),
            const Row(
              children: [
                Text(" WebSeries " , style: TextStyle(
                fontWeight: FontWeight.normal,
                textBaseline: TextBaseline.alphabetic,
              ),
            ),
              ],
            ),
          const SizedBox(
            height: 5,
          ),
          SingleChildScrollView(
          child: 
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: 
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 5,
                    ),
                                
                    Image.network(
                      "https://www.tallengestore.com/cdn/shop/products/PeakyBlinders-NetflixTVShow-ArtPoster_125897c4-6348-41e8-b195-d203700ebcca.jpg?v=1619864555",
                      height:200,
                      width: 170,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Image.network(
                      "https://assetscdn1.paytm.com/images/catalog/product/H/HO/HOMSHERLOCK-HOLHK-P63024784A1CC1B/1563111214645_0..jpg",
                      height:200,
                      width: 170,

                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Image.network(
                      "https://assetscdn1.paytm.com/images/catalog/product/H/HO/HOMSHERLOCK-HOLHK-P63024784A1CC1B/1563111214645_0..jpg",
                      height:200,
                      width: 170,

                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Image.network(
                      "https://www.tallengestore.com/cdn/shop/products/PeakyBlinders-NetflixTVShow-ArtPoster_125897c4-6348-41e8-b195-d203700ebcca.jpg?v=1619864555",
                      height:200,
                      width: 170,
                    
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Image.network(
                      "https://assetscdn1.paytm.com/images/catalog/product/H/HO/HOMSHERLOCK-HOLHK-P63024784A1CC1B/1563111214645_0..jpg",
                      height:200,
                      width: 170,

                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Image.network(
                      "https://pbs.twimg.com/media/EJa01k8XsAI4dQU.jpg",
                      height:200,
                      width: 170,

                    ),
                    
                  ],
                ),

                ),

            ),

            const SizedBox(
                height: 10,
              ),
            const Row(
              children: [
                Text(" Most Popular " , style: TextStyle(
                fontWeight: FontWeight.normal,
                textBaseline: TextBaseline.alphabetic,
              ),
            ),
              ],
            ),
          const SizedBox(
            height: 5,
          ),
          SingleChildScrollView(
          child: 
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: 
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 5,
                    ),
                                
                    Image.network(
                      "https://dbdzm869oupei.cloudfront.net/img/posters/preview/91008.png",
                      height:172,
                      width: 130,
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Image.network(
                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0kR0gMemRl9ylPTzmmuQQVb10vo8n7kXL7BeHkeo_4lmJS56C8-WKIy_GYK12wnEmPlc",
                      height:172,
                      width: 130,

                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Image.network(
                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZ5Cq8kozpWIaq5Aohw4rjKkh_eE7nUkDV5zcHClQaYw&s",
                      height:172,
                      width: 130,
                    
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Image.network(
                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0kR0gMemRl9ylPTzmmuQQVb10vo8n7kXL7BeHkeo_4lmJS56C8-WKIy_GYK12wnEmPlc",
                      height:172,
                      width: 130,

                    ),
                    Image.network(
                      "https://dbdzm869oupei.cloudfront.net/img/posters/preview/91008.png",
                      height:172,
                      width: 130,
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Image.network(
                      "https://i.pinimg.com/564x/02/2c/8c/022c8c3ce9953af6e2daf2b95de33d37--jennifer-lawrence.jpg",
                      height:172,
                      width: 130,
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    
                  ],
                ),

                ),

            ),
          ],
        ),
      ),
    );
  }

}