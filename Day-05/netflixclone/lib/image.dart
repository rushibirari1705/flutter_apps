import 'package:flutter/material.dart';

class Img {
  String url;

  Img({this.url= ""});

  Widget allImg() {

    Widget img = Padding(
      padding: const EdgeInsets.only(right: 20),
      child: Image.network(
        url,
        height: 200,
        width: 280,
        fit: BoxFit.fill,
        
      ),
    );

    return img;
  }
  Widget newImg() {
    Widget img = Padding(
      padding: const EdgeInsets.only(right: 20),
      child: Image.network(
        url,
        height: 280,
        width: 230,
        fit: BoxFit.cover,
      ),
    );

    return img;
  }

  Widget webImg() {

    Widget img = Padding(
      padding: const EdgeInsets.only(right: 20),
      child: Image.network(
        url,
        height: 230,
        width: 180,
        fit: BoxFit.cover,
        
      ),
    );

    return img;
  }



  Widget mostPopImg() {

    Widget img = Padding(
      padding: const EdgeInsets.only(right: 20),
      child: Image.network(
        url,
        height: 170,
        width: 150,
        fit: BoxFit.cover,
      ),
    );

    return img;
  }
}


class DecorFun{
  Widget? x ;
  DecorFun( {this.x});

  Widget cont(){
  Widget cont = Container(
              height: 200,
              width: 320,
              decoration: BoxDecoration(
              border: Border.all(
              width: 2,
              color: Colors.white,
          ),
        ),
      );
        
      return cont;
  }
}