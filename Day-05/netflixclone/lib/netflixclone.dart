import 'package:flutter/material.dart';
import 'image.dart';

class NetflixClone extends StatelessWidget {
  const NetflixClone({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 72, 72, 72),
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: Image.asset(
                "assets/netflix_logo.png",
                height: 45,
              ),
            ),
            Image.asset(
              "assets/netflix_name.jpg",
              height: 45,
              width: 120,
            ),
            const Padding(
              padding: EdgeInsets.only(left: 190),
              child: Icon(
                Icons.menu_rounded,
                size: 30.0,
                color: Colors.white,
              ),
            ),
          ],
        ),
        

      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 70,top: 10,bottom: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    
                      children: [
                        Text("TV shows",
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(width: 50),
                        Text("Movies",
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(width: 50),
                        Text("My List",
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,                             
                              
                            ),
                          ),
                          SizedBox(width: 20),
                      ],
                  ),
                ),
              ],
            ),
            
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const  EdgeInsets.only(left: 0),
                    child:
                      Img(url: "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg")
                            .allImg(),
                  ),

                  Img(url: "https://filmfare.wwmindia.com/content/2022/dec/pathaan11669878737.jpg")
                      .allImg(),
                  Img(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUkW6rTBzfzXWPMK0I6lIgQtTAdmXiOLLoz1Q813eYC8VIepHCnquZsnZUUaUtcPjqfjA&usqp=CAU")
                      .allImg(),
                  Img(url: "https://mir-s3-cdn-cf.behance.net/project_modules/hd/3a62ce49135139.58ac2c8de7a2c.jpg")
                      .allImg(),
                  Img(url: "https://filmfare.wwmindia.com/content/2022/dec/pathaan11669878737.jpg")
                      .allImg(),
                  Img(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUkW6rTBzfzXWPMK0I6lIgQtTAdmXiOLLoz1Q813eYC8VIepHCnquZsnZUUaUtcPjqfjA&usqp=CAU")
                      .allImg(),
                  Img(url: "https://c4.wallpaperflare.com/wallpaper/852/644/1008/alien-movie-poster-sigourney-weaver-movie-poster-wallpaper-preview.jpg")
                      .allImg(),
                ],
              ),
              
            ),

            ),
            
            const SizedBox(height: 10,),
            const Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    left: 15,
                    top: 5,
                    bottom: 5,
                  ),
                  child: Text(
                    " Movies ",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child:
                        Img(url: "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg")
                            .newImg(),
                  ),
                  Img(url: "https://filmfare.wwmindia.com/content/2022/dec/pathaan11669878737.jpg")
                      .newImg(),
                  Img(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUkW6rTBzfzXWPMK0I6lIgQtTAdmXiOLLoz1Q813eYC8VIepHCnquZsnZUUaUtcPjqfjA&usqp=CAU")
                      .newImg(),
                  Img(url: "https://mir-s3-cdn-cf.behance.net/project_modules/hd/3a62ce49135139.58ac2c8de7a2c.jpg")
                      .newImg(),
                  Img(url: "https://filmfare.wwmindia.com/content/2022/dec/pathaan11669878737.jpg")
                      .newImg(),
                  Img(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUkW6rTBzfzXWPMK0I6lIgQtTAdmXiOLLoz1Q813eYC8VIepHCnquZsnZUUaUtcPjqfjA&usqp=CAU")
                      .newImg(),
                  Img(url: "https://c4.wallpaperflare.com/wallpaper/852/644/1008/alien-movie-poster-sigourney-weaver-movie-poster-wallpaper-preview.jpg")
                      .newImg(),
                ],
              ),
              
            ),
            //const SizedBox(height: 20,),
            const Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    left: 15,
                    top: 10,
                    bottom: 5,
                  ),
                  child: Text(
                    " Web Series ",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child:
                        Img(url: "https://i.pinimg.com/originals/f7/b0/73/f7b0731183f30311a4f1740fea7b45fe.jpg")
                            .webImg(),
                  ),
                  Img(url: "https://assets.gadgets360cdn.com/pricee/assets/product/202205/Stranger_Things_4-New-Poster_1652437152.jpg")
                      .webImg(),
                  Img(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUkW6rTBzfzXWPMK0I6lIgQtTAdmXiOLLoz1Q813eYC8VIepHCnquZsnZUUaUtcPjqfjA&usqp=CAU")
                      .webImg(),
                  Img(url: "https://mir-s3-cdn-cf.behance.net/project_modules/hd/3a62ce49135139.58ac2c8de7a2c.jpg")
                      .webImg(),
                  Img(url: "https://static.toiimg.com/photo/104161760/104161760.jpg")
                      .webImg(),
                  Img(url: "https://i.pinimg.com/550x/cb/17/8e/cb178e17efc3d298f06b14a705fe3874.jpg")
                      .webImg(),
                  Img(url: "https://c4.wallpaperflare.com/wallpaper/852/644/1008/alien-movie-poster-sigourney-weaver-movie-poster-wallpaper-preview.jpg")
                      .webImg(),
                ],
              ),
            ),

            const Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    left: 15,
                    top: 5,
                    bottom: 5,
                  ),
                  child: Text(
                    " Popular Movies ",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child:
                        Img(url: "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg")
                            .mostPopImg(),
                  ),
                  Img(url: "https://filmfare.wwmindia.com/content/2022/dec/pathaan11669878737.jpg")
                      .mostPopImg(),
                  Img(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUkW6rTBzfzXWPMK0I6lIgQtTAdmXiOLLoz1Q813eYC8VIepHCnquZsnZUUaUtcPjqfjA&usqp=CAU")
                      .mostPopImg(),
                  Img(url: "https://mir-s3-cdn-cf.behance.net/project_modules/hd/3a62ce49135139.58ac2c8de7a2c.jpg")
                      .mostPopImg(),
                  Img(url: "https://filmfare.wwmindia.com/content/2022/dec/pathaan11669878737.jpg")
                      .mostPopImg(),
                  Img(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUkW6rTBzfzXWPMK0I6lIgQtTAdmXiOLLoz1Q813eYC8VIepHCnquZsnZUUaUtcPjqfjA&usqp=CAU")
                      .mostPopImg(),
                  Img(url: "https://c4.wallpaperflare.com/wallpaper/852/644/1008/alien-movie-poster-sigourney-weaver-movie-poster-wallpaper-preview.jpg")
                      .mostPopImg(),
                ],
              ),
              
            ),
          ],
          
        ),
      ),

      bottomNavigationBar: BottomNavigationBar(
        items:const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home,
            size: 23,
            opticalSize: 20,
            color: Colors.red,
            ),
            tooltip: "Home",
            label: 'Home',
            backgroundColor: Color.fromARGB(255, 22, 9, 9),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search,
            size: 23,
            opticalSize: 20,
            //color: Colors.red,
            ),
            label: 'search',
            tooltip: "search",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.download_outlined,
            size: 23,
            opticalSize: 20,
            //color: Colors.red,
            ),
            label: 'Downloads',
            tooltip: "Downloads",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_add_alt_1,
            size: 23,
            opticalSize: 20,
            //color: Colors.red,
            ),
            label: 'More',
            tooltip: "More",
            
          ),
        ],
      ),
    );
  }
}
