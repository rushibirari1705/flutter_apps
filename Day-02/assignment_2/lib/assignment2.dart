
import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget{
  const Assignment2({super.key});

  @override
  State<Assignment2> createState() => _Assignment2State();

}

class _Assignment2State extends State<Assignment2>{

  bool box1Color = false;
  bool box2Color = false;

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Color Box"),
        backgroundColor: const Color.fromARGB(255, 106, 186, 251),
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment:  MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children:[
            //box1
                Column(
                  children: [
                    Container(
                      height: 150,
                      width: 150,
                      color: box1Color ? Colors.red : Colors.black,
                    ),

                    const SizedBox(
                      height: 20,
                    ),

                    ElevatedButton(
                      onPressed: (){
                        setState(() {
                          box1Color = !box1Color;
                        });
                      },
                      child: const Text("color box 1") ),
                  ],
                ),

                const SizedBox(
                  width: 20,
                ),

                //box2
                Column(
                  children: [
                    Container(
                      height: 150,
                      width: 150,
                      color: box2Color ? Colors.blue : Colors.black,
                    ),

                    const SizedBox(
                      height: 20,
                    ),

                    ElevatedButton(
                      onPressed: (){
                        setState(() {
                          box2Color = !box2Color;
                        });
                      },
                      child: const Text("color box 2") ),
                  ],
                ),
            ],

          ),
        ],

      ),
    );
  }

}