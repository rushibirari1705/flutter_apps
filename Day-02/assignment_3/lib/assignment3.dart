
import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget{
  const Assignment3({super.key});

  @override
  State<Assignment3> createState() => _Assignment3State();

}

class _Assignment3State extends State<Assignment3>{
  
  int? sel_index = 0;

  final List<String> imageList = [
                                  "https://cdn.pixabay.com/photo/2014/02/27/16/10/flowers-276014_640.jpg",
                                  "https://img.freepik.com/free-photo/wide-angle-shot-single-tree-growing-clouded-sky-during-sunset-surrounded-by-grass_181624-22807.jpg",
                                  
    ];
  void showNextImage(){
    setState(() {
      sel_index = sel_index! + 1;
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title : const Text("Display Images"),
      ),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                imageList[sel_index!],
                width: 350,
                height: 350,
              ),

              const SizedBox(
                height: 20,
              ),

              ElevatedButton(
                onPressed: showNextImage,
                child: const Text("Next->"),
              ),

              const SizedBox(
                height: 20,
              ),

              ElevatedButton(
                onPressed: (){
                  setState((){
                      sel_index = 0;
                  } 
                  );
                } ,
                child: const Text("Reset")
              ),
            ],
          ),
      ),

    );
  }
}