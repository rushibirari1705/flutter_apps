import "package:flutter/material.dart";


void main() => runApp(const MyApp());

class MyApp extends StatelessWidget{
  const MyApp({super.key});

  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      home: FlagAssignment(),
    );
  }
}

class FlagAssignment extends StatelessWidget{

  const FlagAssignment({super.key});
  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: const Text("Indian Flag "),

      ),

      body: Container(
        height: double.infinity,
        width: double.infinity,
        child:Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
                  Column( 
                    children: [
                      const Row(
                        children: [
                          CircleAvatar(
                            backgroundColor: Color.fromARGB(255, 0, 0, 0),
                            minRadius: 10,
                          ),  
                        ],
                      ),
                      Container(
                        height: 550,
                        color: const Color.fromARGB(255, 62, 62, 62),
                        width: 15,
                      ),
                    ],
                  ),

                  const SizedBox(
                    width: 2,
                  ),
                  
                  Padding(
                    padding: const EdgeInsets.only(top : 23),
                    child: Column(
                      children: [
                        Container(
                          height: 60,
                          width: 300,
                          color: Colors.orangeAccent,
                    
                        ),
                        Container(
                          height: 60,
                          width: 300,
                          color: const Color.fromARGB(255, 255, 255, 255),
                          child:Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.network(
                                "https://www.shutterstock.com/image-vector/blue-ashoka-wheel-indian-symbol-600nw-417259699.jpg",
                                height: 50,
                                width: 50,
                              ),
                    
                            ],
                            ),
                            
                    
                        ),
                        Container(
                          height: 60,
                          width: 300,
                          color: const Color.fromARGB(255, 11, 155, 16),
                    
                        ),
                      ],
                    ),
                  ),
                ],          
        ),
      ),
    );
  }
}