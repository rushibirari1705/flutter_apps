

import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget{
  const Assignment1({super.key});

  @override
  State<Assignment1> createState() => _Assignment1State();
  
}

class _Assignment1State extends State<Assignment1>{

  final List<int> li = [123321,789,6666,5456,343];
  final List<int> l2 = [145,789,236,5456,40585];
  final List<int> l3 = [1,153,236,371,456];

  int? count = 0;
  int? index = 0;
  int? scount = 0;
  int? acount = 0;

  void isPalindrome(){
      count = 0;
      for (int i = 0; i < li.length; i++){
        int num = li[i];
        int val = li[i];
        int rev = 0;
        while(num != 0){

          int rem = num % 10;
          rev = rev * 10 + rem;
          num = num ~/ 10;

        }

        if(rev == val){
          count = count! + 1;
        }  
      }
      
  }
  void checkPalindrome(){
    setState(() {
        isPalindrome();
    });
  }
  //
  void isStrong(){
    scount = 0;
    for(int i = 0; i < l2.length; i++){
        int num = l2[i];
        int sumfact = 0;
        int temp = l2[i];

        while(temp != 0){
          int rem = temp % 10;
          int fact = 1;
          for(int j = 1; j <= rem; j++){
            fact = fact * j;

          }
          sumfact += fact;
          temp = temp ~/10;
        }
        if(sumfact == num){
          scount = scount! +  1;
        }

    }

  }
  void checkStrong(){
    setState(() {
      isStrong();
    });
  }

  void isArmStrong(){
      acount = 0;
      for(int i = 0; i < l3.length;i++){
          int num = l3[i];
          int sum = 0;
          int temp = l3[i];
          
          int cnt = 0;
          for(int i = num; i != 0; i ~/= 10){
            cnt++;
          }

          while(temp != 0){
            int fact = 1;
            for(int j = 1 ; j <= cnt ; j++){
              fact = fact * (temp % 10);
            }
            sum += fact;
            temp ~/= 10;
          }

          if(sum == num) acount = acount! + 1;
      }

  }
  void checkArmstrong(){
    setState(() {
      isArmStrong();
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text(" Check counts of numbers"),
        backgroundColor: const Color.fromARGB(22, 138, 230, 255),
      ),

      body:

      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment:  MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children:[
              Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                  const Text(" click to check palindrome",style: TextStyle(color: Color.fromARGB(255, 7, 7, 7),fontSize: 20),
                  ),

                  const SizedBox(
                    height: 20,
                  ),

                  Container(
                    height: 50,
                    width: 50,
                    color: const Color.fromARGB(255, 230, 230, 230),
                    child: Center(
                      child: Text("$count",style: const TextStyle(
                                color: Color.fromARGB(255, 0, 0, 0),
                                fontSize: 18,
                                fontWeight:FontWeight.bold,
                                  ),
                              ),
                    ),
                ),

                  
                  const SizedBox(
                    height: 20,
                  ),

                  ElevatedButton(
                    onPressed: checkPalindrome,
                    style:ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromARGB(255, 255, 208, 53),
                      foregroundColor: const Color.fromARGB(255, 8, 8, 8),
                    ),
                    child: const Text("Calculate Palindrome") ,
                    
                  ),
              ],
            ),
            const SizedBox(
              width: 30,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                  const Text(" Click to check Strong",style: TextStyle(color: Color.fromARGB(255, 7, 7, 7),fontSize: 20),),

                  const SizedBox(
                    height: 20,
                  ),


                  Container(
                    height: 50,
                    width: 50,
                    color: const Color.fromARGB(255, 230, 230, 230),
                    child: Center(
                      child: Text("$scount",style: const TextStyle(
                                color: Color.fromARGB(255, 0, 0, 0),
                                fontSize: 18,
                                fontWeight:FontWeight.bold,
                                  ),
                              ),
                  ),
                ),
                  const SizedBox(
                    height: 20,
                  ),

                  ElevatedButton(
                    onPressed: checkStrong,
                    style:ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromARGB(255, 138, 234, 255),
                      foregroundColor: const Color.fromARGB(255, 8, 8, 8),
                    ),
                    child: const Text("Calculate Strong") , 
                  ),
              ],
            ),
            const SizedBox(
              width: 30,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                  const Text(" Click to check Armstrong",style: TextStyle(color: Color.fromARGB(255, 7, 7, 7),fontSize: 20),),

                  const SizedBox(
                    height: 20,
                  ),

                  Container(
                    height: 50,
                    width: 50,
                    color: Color.fromARGB(255, 230, 230, 230),
                    child: Center(
                      child: Text("$acount",style: const TextStyle(
                                color: Color.fromARGB(255, 0, 0, 0),
                                fontSize: 18,
                                fontWeight:FontWeight.bold,
                                  ),
                              ),
                  ),
                ),
                  const SizedBox(
                    height: 20,
                  ),

                  ElevatedButton(
                    onPressed: checkArmstrong,
                    style:ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromARGB(255, 255, 178, 250),
                      foregroundColor: const Color.fromARGB(255, 8, 8, 8),
                    ),
                    child: const Text("Calculate Armstrong") , 
                  ),
              ],
            ),
          ],
        ),
      ],
    ),
    );
  }

}