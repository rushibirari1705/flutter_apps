
// Assignment 1:
// creating a widget which shows the Palindrome numbers from list.

import 'package:flutter/material.dart';
import 'assignment1.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget{
    const MyApp({super.key});

    @override
    Widget build(BuildContext context){
      return const MaterialApp(
        home: Assignment1(),
      );
    }

}
