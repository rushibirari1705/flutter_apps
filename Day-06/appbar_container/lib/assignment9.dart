
import 'package:flutter/material.dart';

class Assignment9 extends StatelessWidget {
  const Assignment9({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child:
          
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 300,
                width: 300,
                //color: const Color.fromARGB(255, 144, 205, 255),
                decoration:BoxDecoration(
                  color: const Color.fromARGB(255, 144, 205, 255),
                  border: Border.all(
                    color: Colors.red,
                    width: 5,
                  ),
                  borderRadius: const BorderRadius.all(
                      Radius.circular(80),
                  ),
                ),
              ),
            ],
          )
          ,
      ),
    );

  }

}