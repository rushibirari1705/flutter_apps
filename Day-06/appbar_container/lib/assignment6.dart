
import 'package:flutter/material.dart';

class Assignment6 extends StatelessWidget {
  const Assignment6({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      // appBar: AppBar(
      //     backgroundColor: Colors.deepPurple,
      // ),

      body: 
      SingleChildScrollView(
        child: Center(
          child:
            Column(
              mainAxisAlignment: MainAxisAlignment.center,

              children: [
                Column(
                      children: [
                        Container(
                          height: 180,
                          width: 250,
                          color: const Color.fromARGB(255, 113, 191, 255),
                        ),
                        const SizedBox(height: 10),
                      
                        Container(
                          height: 180,
                          width: 250,
                          color: const Color.fromARGB(255, 255, 244, 125),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          height: 180,
                          width: 250,
                          color: const Color.fromARGB(255, 22, 139, 236),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          height: 180,
                          width: 250,
                          color: const Color.fromARGB(255, 215, 96, 255),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          height: 180,
                          width: 250,
                          color: const Color.fromARGB(255, 0, 140, 255),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          height: 180,
                          width: 250,
                          color: const Color.fromARGB(255, 255, 120, 62),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          height: 180,
                          width: 250,
                          color: const Color.fromARGB(255, 113, 191, 255),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          height: 180,
                          width: 250,
                          color: const Color.fromARGB(255, 255, 244, 125),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          height: 180,
                          width: 250,
                          color: const Color.fromARGB(255, 160, 255, 113),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          height: 180,
                          width: 250,
                          color: const Color.fromARGB(255, 252, 102, 102),
                        ),
                        const SizedBox(height: 10),
                      
                      ],
                  ),
              ],
            ),
        ),
      ),
    );

  }

}