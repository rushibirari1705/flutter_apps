
import 'package:flutter/material.dart';

class Assignment4 extends StatelessWidget {
  const Assignment4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      // appBar: AppBar(
      //     backgroundColor: Colors.deepPurple,
      // ),

      body: Center(
        child:
          
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    height: 200,
                    width: 360,
                    color: const Color.fromARGB(255, 113, 191, 255),
                  ),
                  Container(
                    height: 200,
                    width: 360,
                    color: const Color.fromARGB(255, 255, 244, 125),
                  ),
                ],
              ),
            ],
          )
          ,
      ),
    );

  }

}