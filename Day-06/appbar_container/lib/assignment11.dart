
import "package:flutter/material.dart";

class Assignment11 extends StatelessWidget {
  const Assignment11({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Assignment 11 "),
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children:[
            Container(
              height: 100,
              width: 100,
              color: Colors.red,
            ),
            const SizedBox(
              width: 100,
            ),
            Container(
              height: 100,
              width: 100,
              color: const Color.fromARGB(255, 247, 151, 34),
            ),
            const SizedBox(
              width: 100,
            ),
            Container(
              height: 100,
              width: 100,
              color: const Color.fromARGB(255, 79, 108, 255),
            ),
          ],
        ),
        const SizedBox(
              width: 100,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children:[
            Container(
              height: 100,
              width: 100,
              color: Colors.red,
            ),
            const SizedBox(
              width: 100,
            ),
            Container(
              height: 100,
              width: 100,
              color: const Color.fromARGB(255, 247, 151, 34),
            ),
            const SizedBox(
              width: 100,
            ),
            Container(
              height: 100,
              width: 100,
              color: const Color.fromARGB(255, 79, 108, 255),
            ),
          ],
        ),       

      ],

      )
    );

  }

}