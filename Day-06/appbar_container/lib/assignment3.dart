
import 'package:flutter/material.dart';

class Assignment3 extends StatelessWidget {
  const Assignment3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
          title: const Text( " Hello Core2web " , 
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
            
          ),
          backgroundColor: Colors.deepPurple,
      ),

      body: Center(
        child:
          
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 200,
                width: 360,
                color: const Color.fromARGB(255, 113, 191, 255),
              ),
            ],
          )
          ,
      ),
    );

  }

}