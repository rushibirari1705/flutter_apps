
import 'package:flutter/material.dart';

class Assignment5 extends StatelessWidget {
  const Assignment5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child:
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "assets/c1.jpeg",
                width: 150,
                height: 150,
              ),
              const SizedBox(height: 25),
              Image.asset(
                "assets/c2.jpeg",
                width: 150,
                height: 150,
              ),
              const SizedBox(height: 25),
              Image.asset(
                "assets/c3.jpg",
                width: 150,
                height: 150,
              ),
              const SizedBox(height: 25),

            ],
          )
          ,
      ),
    );

  }

}