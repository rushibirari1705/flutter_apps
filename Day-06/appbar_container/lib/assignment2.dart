
import 'package:flutter/material.dart';

class Assignment2 extends StatelessWidget {
  const Assignment2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
          title: const Text( " Assignment 2" , 
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
            
          ),

          centerTitle: true,
          backgroundColor: const Color.fromARGB(197, 239, 185, 255),
          actions: const  [
              Icon(
                  Icons.favorite_outline_rounded,
              ),
              SizedBox(
                width: 20,
              ),
              Icon(
                  Icons.maps_ugc_rounded,
              ),
              SizedBox(
                width: 20,
              ),
          ],  
      ),
    );

  }

}