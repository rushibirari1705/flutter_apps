

import 'package:flutter/material.dart';

class Assignment7 extends StatelessWidget {
  const Assignment7({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          child: 
          Center(
            child:
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.network(
                    "https://media.istockphoto.com/id/1189903200/photo/red-generic-sedan-car-isolated-on-white-background-3d-illustration.jpg?s=612x612&w=0&k=20&c=uRu3o_h5FVljLQHS9z0oyz-XjXzzXN_YkyGXwhdMrjs=",
                    width: 150,
                    height: 180,
                  ),
                  
                  const SizedBox(height: 25),
                  Image.network(
                    "https://images.pexels.com/photos/39501/lamborghini-brno-racing-car-automobiles-39501.jpeg?cs=srgb&dl=pexels-pixabay-39501.jpg&fm=jpg",
                    width: 150,
                    height: 180,
                  ),
                  const SizedBox(height: 25),
                  Image.network(
                    "https://images.unsplash.com/photo-1566008885218-90abf9200ddb?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8dmVoaWNsZXxlbnwwfHwwfHx8MA%3D%3D",
                    width: 150,
                    height: 180,
                  ),
                  const SizedBox(height: 25),
                  Image.network(
                    "https://images.pexels.com/photos/39501/lamborghini-brno-racing-car-automobiles-39501.jpeg?cs=srgb&dl=pexels-pixabay-39501.jpg&fm=jpg",
                    width: 150,
                    height: 180,
                  ),
                  const SizedBox(height: 25),
                  Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuttobzAz6RMUMuq3ADURevodiQEYMIJSDdQ&usqp=CAU",
                    width: 150,
                    height: 180,
                  ),
                  const SizedBox(height: 25),
                  Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8yhNNGTb_BoH5XQW2TExN7cBsWAn4z8_Kv6zJBbUv5bFhJP1hrHps8J5wrH0UwlL1aJ4&usqp=CAU",
                    width: 150,
                    height: 180,
                  ),
                  const SizedBox(height: 25),
                  Image.network(
                    "https://images.unsplash.com/photo-1566008885218-90abf9200ddb?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8dmVoaWNsZXxlbnwwfHwwfHx8MA%3D%3D",
                    width: 150,
                    height: 180,
                  ),
                  const SizedBox(height: 25),

          
                ],
              )
              ,
          ),
      ),
    );

  }

}